import numpy as np
import sys
import glob

from astropy.table import Table
from astropy.table import vstack as Table_vstack
from astropy.io import fits

def concatenate_catalogues(config):

  catfile_list = config.get('source_finder','catalogue_filename')

  cat_list = []

  for catfile in catfile_list:
    cat_list.append(Table.read(catfile, format='fits'))

  concat_cat = Table_vstack(cat_list)
  
  catfile_single = catfile_list[0].rsplit('_', 1)[0]+'.concat.srl.fits'

  concat_cat.write(catfile_single, overwrite=True)

  config.set('source_finder','concatenated_catalogue_filename', catfile_single)

def runSourceFinder(config):

  if config.get('source_finder', 'mode')=='pybdsf':
    runPyBDSF(config)

def runPyBDSF(config):
  import bdsf
  catfile_list = config.get('source_finder', 'catalogue_filename')
  for i,imagefile in enumerate(config.get('input', 'input_image_name')):
    image_dir = config.get('input', 'input_directory')
    img = bdsf.process_image(image_dir+imagefile, adaptive_rms_box = config.getboolean('pybdsf', 'adaptive_rms_box'),
                                        adaptive_thresh = config.getfloat('pybdsf', 'adaptive_thresh'),
                                        rms_box_bright = [int(x) for x in config.get('pybdsf', 'rms_box_bright').split(',')],
                                        rms_box = [int(x) for x in config.get('pybdsf', 'rms_box').split(',')],
                                        catalog_type=config.get('pybdsf', 'catalog_type'))

    if config.getboolean('pybdsf', 'write_fits'):
      img.write_catalog(outfile=catfile_list[i],
                        format='fits', clobber=True)
    if config.getboolean('pybdsf', 'write_ds9'):
      img.write_catalog(outfile=catfile_list[i].rstrip('.fits')+'.reg',
                        format='ds9', clobber=True)
    if config.getboolean('pybdsf', 'write_rms'):
      img.export_image(outfile=catfile_list[i].rstrip('.fits')+'.rms.fits', img_format='fits', img_type='rms')
