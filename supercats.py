import numpy as np
import ConfigParser

import sys
import pdb

def updateFilenames(config):

  input_dir = config.get('input','input_directory')
  output_dir = config.get('pipeline', 'output_dir')
  plot_dir = config.get('pipeline', 'plot_dir')
  input_image_list = config.get('input','input_image_list')
  
  if not isinstance(input_image_list, (list,)):
    config.set('input', 'input_image_list', [config.get('input', 'input_image_list')])
    config.set('input', 'input_image_filename', [input_image_list][0])
  else:
    print('ERROR! Cannot deal with multiple input images yet!')
    return -1
    
  input_image_filename = config.get('input', 'input_image_filename')

  rms_map_fname = output_dir+input_image_filename.rstrip('.fits')+'_rms{0}x{0}.fits'.format(config.getint('rms_map', 'rebin_factor'))
  config.set('rms_map', 'rms_map_filename', rms_map_fname)

  mask_map_fname = output_dir+input_image_filename.rstrip('.fits')+'_mask{0}x{0}.fits'.format(config.getint('rms_map', 'rebin_factor'))
  config.set('rms_map', 'mask_filename', mask_map_fname)


def runSuperCats(config):

  updateFilenames(config)

  print('######################################')
  print('       START IMAGE {0}       '.format(config.get('input', 'input_image_filename')))
  print('######################################')
  
  if config.getboolean('pipeline', 'do_rms_map'):
    from rms_map import runRMSMap
    runRMSMap(config)

  if config.getboolean('pipeline', 'do_slicing'):
    from slicer import runSlicer
    runSlicer(config)
  
  catfile_list = []
  for imagefile in config.get('input', 'input_image_list'):
    catfile_list.append(config.get('pipeline', 'output_dir')+imagefile.rstrip('.fits')+'.'+config.get('pybdsf', 'catalog_type')+'.fits')

  config.set('source_finder','catalogue_filename', catfile_list)

  if config.getboolean('pipeline', 'do_source_finder'):
    from source_finder import runSourceFinder
    runSourceFinder(config)

  if len(config.get('source_finder','catalogue_filename'))>1:
    from source_finder import concatenate_catalogues
    concatenate_catalogues(config)
    
  if config.getboolean('pipeline', 'do_process_catalogue'):
    from process_catalogue import runCatalogueProcessing
    runCatalogueProcessing(config)
    
if __name__=='__main__':

  config_filename = sys.argv[-1]
  config = ConfigParser.ConfigParser()
  config.read(config_filename)
  
  runSuperCats(config)
