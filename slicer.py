import numpy as np
from astropy.io import fits
from astropy.wcs import WCS
import sys

def slice_fits(data, w, xmin, xmax, ymin, ymax, file_out, hist, master_header):
    """ Cuts out xmin:xmax,ymin:ymax subarray from data and
    adjust wcs accordingly and writes out to file file file_out
    """

    dcut = data[xmin:xmax, ymin:ymax]
    wcut = w[xmin:xmax, ymin:ymax]
    
    head = wcut.to_header()
    PriHdu = fits.PrimaryHDU(header=head,data=dcut)
    for h in hist:
        PriHdu.header['HISTORY'] = h
    
    if 'BMAJ' in master_header.keys():
      PriHdu.header['BMAJ'] = master_header['BMAJ']
      PriHdu.header['BMIN'] = master_header['BMIN']
      PriHdu.header['BPA'] = master_header['BPA']
      PriHdu.header['BUNIT'] = master_header['BUNIT']
    elif 'BMAJ=' in master_header['HISTORY'][-2].split(' '):
      # Oh, yeah, sure, lets not put the beam information in the locations
      # specified by the FITS standard, lets put it in the history instead in
      # a place which is really hard to access programmatically.
      hist_split = master_header['HISTORY'][-2].split(' ')
      PriHdu.header['BMAJ'] = float(hist_split[6])
      PriHdu.header['BMIN'] = float(hist_split[9])
      PriHdu.header['BPA'] = float(hist_split[11])
      PriHdu.header['BUNIT'] = master_header['BUNIT']
    else:
      print('CANNOT FIND BEAM INFORMATION IN FILE TO SLICE!')
      return -1
    
    PriHdu.header['CTYPE3'] = master_header['CTYPE3']
    PriHdu.header['CRPIX3'] = master_header['CRPIX3']
    PriHdu.header['CRVAL3'] = master_header['CRVAL3']
    PriHdu.header['CDELT3'] = master_header['CDELT3']
    PriHdu.writeto(file_out, overwrite=True)

def runSlicer(config):

###################################################################
#
# Main Prog
#
# eg call
#
# python fits_slice Mosaic.fits 4 100 Mosaic_Split
#
# will split Mosaic.fits into 16 subfile 4x4 with a 100 pixel overlap
# on the internal boarders
    
    data_dir = config.get('input', 'input_directory')
    input_image_name = config.get('input', 'input_image_filename')    # Input map
    nside = config.getint('slicer', 'nside')  # Split nside times along an axis
    ovlap = config.getint('slicer', 'overlap')   # Number of pixels to overlap at edges
    oname = config.get('slicer', 'output_prefix')        # Output prefix, will add map number
    
    hdu = fits.open(data_dir+input_image_name)
    data = hdu[0].data
    data.shape = (data.shape[2],data.shape[3])
    hist = hdu[0].header['HISTORY']

    w = WCS(hdu[0].header)
    w = w.dropaxis(2)
    w = w.dropaxis(2)

    nxpix = data.shape[0]/nside
    nypix = data.shape[1]/nside

    fname_list = []

    nmap = 0
    for i in range(nside):

        xmin = i*nxpix - ovlap/2
        if xmin<1: xmin = 1

        xmax = (i+1)*nxpix + ovlap/2
        if xmax>data.shape[0]: xmax = data.shape[0]

        for j in range(nside):

            ymin = j*nypix - ovlap/2
            if ymin<1: ymin = 1

            ymax = (j+1)*nypix + ovlap/2
            if ymax>data.shape[1]: ymax = data.shape[1]
            fname = input_image_name.rstrip('.fits')+'_'+"%s_%3.3i.fits" % (oname,nmap)
            fname_list.append(fname)

            print "Writing",fname
            slice_fits(data,w,xmin,xmax,ymin,ymax,data_dir+fname,hist,hdu[0].header)

            nmap += 1
    config.set('input', 'input_image_name', fname_list)
    print "Finished"
