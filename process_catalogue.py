import numpy as np
import pdb
import copy
import sys

from astropy.table import Table
from astropy.io import fits
from astropy import wcs

from matplotlib import pyplot as plt
from matplotlib import rc

rc('text', usetex=True)
rc('font', family='serif')
rc('font', size=11)


def generate_names(config):

  input_image_filename = config.get('input', 'input_directory')+config.get('input', 'input_image_filename')
  image_header = fits.getheader(input_image_filename)
  image_wcs = wcs.WCS(image_header)
  if (image_wcs.naxis == 4):
    image_wcs = image_wcs.dropaxis(2)
    image_wcs = image_wcs.dropaxis(2)

  ra = []
  dc = []
  cat_filename = config.get('source_finder', 'catalogue_filename')

  '''
  if not isinstance(cat_filename, (list,)):
    cat_filename = [cat_filename]
  
  if len(cat_filename) > 1:
    cat_filename = config.get('source_finder', 'concatenated_catalogue_filename')
  '''

  hdu = fits.open(cat_filename)
  cat = Table.read(cat_filename, format='fits')
  ra.append(hdu[1].data.RA)
  dc.append(hdu[1].data.DEC)

  ra = np.concatenate(ra)
  dc = np.concatenate(dc)

  if 'Source_id' in cat.colnames:
    cat['Source_id'] = cat['Source_id'].astype('S27')
  else:
    cat['Source_id'] = np.ones(len(cat), dtype='S27')

  for i,source in enumerate(cat):
    sys.stdout.write('\rRenaming sources... {0}/{1} ({2:.2f}%).'.format(i, len(cat), 100*float(i)/float(len(cat))))
    y, x = image_wcs.wcs_world2pix(source['RA'], source['DEC'], 1)
    source_coord = wcs.utils.pixel_to_skycoord(y,x, image_wcs)
    source_name_ra = str(int(source_coord.ra.hms.h)).zfill(2)+str(int(source_coord.ra.hms.m)).zfill(2)+('%.2f' % source_coord.ra.hms.s).zfill(5)
    source_name_dec = str(int(source_coord.dec.dms.d)).zfill(2)+str(int(source_coord.dec.dms.m)).zfill(2)+('%.2f' % source_coord.dec.dms.s).zfill(5)
    source_name_pos = 'J'+source_name_ra+'+'+source_name_dec
    outprefix = config.get('pipeline', 'source_prefix')
    sname = outprefix+'-'+source_name_pos
    source['Source_id'] = sname
    #print(len(sname), sname)
    if len(sname)<27:
      pdb.set_trace()
    sys.stdout.flush()
  cat.write(cat_filename, format='fits', overwrite=True)

def mask_catalogue(config):

  if config.has_option('process_catalogue', 'external_mask_file'):
    maskfile = config.get('process_catalogue', 'external_mask_file')
  else:
    maskfile = config.get('rms_map', 'mask_filename')
    
  plot_dir = config.get('pipeline', 'plot_dir')

  ra = []
  dc = []
  cat_filename = config.get('source_finder', 'catalogue_filename')
  #if len(cat_filename) > 1:
  #  cat_filename = config.get('source_finder', 'concatenated_catalogue_filename')
  hdu = fits.open(cat_filename)
  cat = Table.read(cat_filename, format='fits')

  ra.append(hdu[1].data.RA)
  dc.append(hdu[1].data.DEC)

  ra = np.concatenate(ra)
  dc = np.concatenate(dc)

  if config.has_option('process_catalogue', 'mask_ra_min'):
    ra_min = config.getfloat('process_catalogue', 'mask_ra_min')
    ra_max = config.getfloat('process_catalogue', 'mask_ra_max')
    dec_min = config.getfloat('process_catalogue', 'mask_dec_min')
    dec_max = config.getfloat('process_catalogue', 'mask_dec_max')

    ra_dec_cut = (ra<ra_min)+(ra>ra_max)+(dc<dec_min)+(dc>dec_max)
  else:
    ra_dec_cut = np.zeros_like(hdu[1].data.RA, dtype=bool)

  f1 = fits.open(maskfile)
  w1 = wcs.WCS(f1[0].header)

  #if (w1.naxis == 4):
    #image = image[0,0]
  w1 = w1.dropaxis(2)
  w1 = w1.dropaxis(2)

  m = f1[0].data

  x_arr = []
  y_arr = []

  x_fail_arr = []
  y_fail_arr = []

  rows_to_remove = []

  rkeep = open('%s_unmasked.reg' % cat_filename[:-5],'w')

  rf = open('%s_masked.reg' % cat_filename[:-5],'w')

  rf.write('# Region file format: DS9 version 4.1\nglobal color=red dashlist=8 3 width=3 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\nJ2000\n')
  rkeep.write('# Region file format: DS9 version 4.1\nglobal color=green dashlist=8 3 width=3 font="helvetica 10 normal roman" select=1 highlite=1 dash=0 fixed=0 edit=1 move=1 delete=1 include=1 source=1\nJ2000\n')

  for i in np.arange(len(ra)):
    y,x = w1.all_world2pix(ra[i], dc[i], 1)
    x = int(x+0.5)
    y = int(y+0.5)
    x_arr.append(x)
    y_arr.append(y)
#    print i,x,y
    
    try:
      maskvalue = m[x,y]
      if(m[x,y]!=1) or ra_dec_cut[i]:
        x_fail_arr.append(x)
        y_fail_arr.append(y)
        rows_to_remove.append(i)
        rf.write("circle(%f,%f,0.30')\n" % (ra[i],dc[i]))
      else:
        rkeep.write("circle(%f,%f,0.30')\n" % (ra[i],dc[i]))
    except IndexError:
      x_fail_arr.append(x)
      y_fail_arr.append(y)
      rows_to_remove.append(i)
      rf.write("circle(%f,%f,0.30')\n" % (ra[i],dc[i]))
    
  x_arr = np.array(x_arr)
  y_arr = np.array(y_arr)
  x_fail_arr = np.array(x_fail_arr)
  y_fail_arr = np.array(y_fail_arr)

  cat.remove_rows(rows_to_remove)
  cat.write(cat_filename.rstrip('.fits')+'.masked.fits', format='fits', overwrite=True)
  '''
  plt.close('all')
  plt.figure(1, figsize=(9, 7.5))
  plt.imshow(m[::10,::10], origin='lower')
  plt.plot(x_arr/10, y_arr/10, 'mo')
  plt.plot(x_fail_arr/10, y_fail_arr/10, 'k+')
  plt.xlim([0,m.shape[0]/10])
  plt.ylim([0,m.shape[1]/10])
  mask_plot_filename = plot_dir+input_image_name.rstrip('.fits')+'_catalogue_masking.png'
  plt.title(input_image_name.split('/')[-1].replace('_', '_'))
  plt.axis('off')
  plt.savefig(mask_plot_filename, dpi=300, bbox_inches='tight')
  '''
  rf.close()
  rkeep.close()

  return cat

def makeRatioPlot(config, cat, resolved_cut):

  peak_snr = cat['Peak_flux']/cat['Resid_Isl_rms']
  int_peak_ratio = cat['Total_flux']/cat['Peak_flux']

  plt.figure(1, figsize=(4.5, 3.75))
  plt.plot(peak_snr, int_peak_ratio, 'o', color='powderblue')
  plt.plot(peak_snr[resolved_cut], int_peak_ratio[resolved_cut], 'o', color='lightcoral')
  plt.xscale('log')
  plt.legend(['$\mathrm{All}$', '$\\mathrm{Maj_{DC} \, & \, Min_{DC}} > 0$'], loc='upper right', fontsize='small')
  plt.xlabel('$S_{\\rm peak}/\sigma_{\\rm Resid\_Isl\_rms}$')
  plt.ylabel('$S_{\\rm int}/S_{\\rm peak}$')
  plt.axvline(5, color='k', linestyle='--')
  plt.axhline(1, color='k', linestyle='--')
  x = np.linspace(peak_snr.min(),peak_snr.max(), 10)
  y1 = np.ones_like(x)*(int_peak_ratio - 1).min()
  plt.fill_between(x, 1+y1, 1+abs(y1), alpha=0.3, color='k', linestyle='None', edgecolor='None', linewidth=0)
  plt.xlim([peak_snr.min(),peak_snr.max()])
  plot_dir = config.get('pipeline', 'plot_dir')
  cat_filename = config.get('source_finder', 'catalogue_filename')
  cat_filename_base = cat_filename.rstrip('.fits')
  plt.savefig(plot_dir+cat_filename_base+'_isl_rms.png', bbox_style='tight', dpi=300)

def runCatalogueProcessing(config):
  
  cat_filename = config.get('source_finder', 'catalogue_filename')
  if len(cat_filename) > 1:
    cat_filename = config.get('source_finder', 'concatenated_catalogue_filename')
  cat_filename_base = cat_filename.rstrip('.fits')
  cat = Table.read(cat_filename, format='fits')
  
  input_image_filename = config.get('input', 'input_directory')+config.get('input', 'input_image_filename')
  image_header = fits.getheader(input_image_filename)
  image_wcs = wcs.WCS(image_header)
  if (image_wcs.naxis == 4):
    image_wcs = image_wcs.dropaxis(2)
    image_wcs = image_wcs.dropaxis(2)
  
  if 'BMAJ' in image_header.keys():
    print('BMAJ Okay.')
  elif 'BMAJ=' in image_header['HISTORY'][-2].split(' '):
    # Oh, yeah, sure, lets not put the beam information in the locations
    # specified by the FITS standard, lets put it in the history instead in
    # a place which is really hard to access programmatically.
    hist_split = image_header['HISTORY'][-2].split(' ')
    image_header['BMAJ'] = float(hist_split[6])
    image_header['BMIN'] = float(hist_split[9])
    image_header['BPA'] = float(hist_split[11])
  else:
    print('CANNOT FIND BEAM INFORMATION IN FILE TO SLICE!')
    return -1

  if config.getboolean('process_catalogue', 'apply_mask'):
    cat = mask_catalogue(config)

  int_peak_ratio = cat['Total_flux']/cat['Peak_flux']
  int_peak_ratio_cut = int_peak_ratio > abs((int_peak_ratio - 1).min())

  resolved_cut = (cat['DC_Min']>0)*(cat['DC_Maj']>0)*(cat['Maj']>image_header['BMAJ'])

  print('Raw catalogue: {0} sources'.format(len(cat)))

  if config.getboolean('process_catalogue', 'write_real_cat'):
    real_cat_filename = cat_filename_base+'.real.fits'
    config.set('process_catalogue', 'real_cat_filename', real_cat_filename)
    real_cat = cat[int_peak_ratio_cut]
    real_cat.write(real_cat_filename, format='fits', overwrite=True)
    print('Real catalogue: {0} sources'.format(len(real_cat)))

  if config.getboolean('process_catalogue', 'write_resolved_cat'):
    resolved_cat_filename = cat_filename_base+'.resolved.fits'
    config.set('process_catalogue', 'resolved_cat_filename', resolved_cat_filename)
    resolved_cat = cat[int_peak_ratio_cut*resolved_cut]
    resolved_cat.write(resolved_cat_filename, format='fits', overwrite=True)
    print('Resolved catalogue: {0} sources'.format(len(resolved_cat)))

  if config.getboolean('process_catalogue', 'write_sfg_cat'):
    
    non_sfg_list_filename = config.get('process_catalogue', 'non_sfg_list_filename')
    bad_list = np.genfromtxt(non_sfg_list_filename, dtype=str)

    sfg_cat = copy.copy(cat)

    sfg_cat_filename = cat_filename_base+'.sfg.fits'
    sfg_highflux_cat_filename = cat_filename_base+'.sfg.highflux.fits'
    config.set('process_catalogue', 'sfg_cat_filename', sfg_cat_filename)
    sfg_cat = sfg_cat[int_peak_ratio_cut*resolved_cut]

    rows_to_remove = []
    rows_to_remove_real_cat = []

    for i,source in enumerate(real_cat):
      sname = source['Source_id']
      if sname in bad_list:
        rows_to_remove_real_cat.append(i)

    for i,source in enumerate(sfg_cat):
      #y, x = image_wcs.wcs_world2pix(source['RA'], source['DEC'], 1)
      #source_coord = wcs.utils.pixel_to_skycoord(y,x, image_wcs)
      #source_name_ra = str(int(source_coord.ra.hms.h))+str(int(source_coord.ra.hms.m))+('%.2f' % source_coord.ra.hms.s)
      #source_name_dec = str(int(source_coord.dec.dms.d))+str(int(source_coord.dec.dms.m))+('%.2f' % source_coord.dec.dms.s)
      #source_name_pos = 'J'+source_name_ra+'+'+source_name_dec
      #outprefix = 'SCL-EM'
      #sname = outprefix+'-'+source_name_pos
      sname = source['Source_id']
      if sname in bad_list:
        #print('Skipping source {0}'.format(sname))
        rows_to_remove.append(i)


    sfg_cat.remove_rows(rows_to_remove)
    print('SFG catalogue: {0} sources'.format(len(sfg_cat)))
    flux_cut = sfg_cat['Total_flux'] > config.getfloat('process_catalogue', 'flux_cut')
    print('Flux-cut catalogue: {0} sources'.format(len(sfg_cat[flux_cut])))
    sfg_cat.write(sfg_cat_filename, format='fits', overwrite=True)
    sfg_cat[flux_cut].write(sfg_highflux_cat_filename, format='fits', overwrite=True)
    makeRatioPlot(config, cat, resolved_cut)

    # make resolved column
    resolved_cut_real_cat = (real_cat['DC_Min']>0)*(real_cat['DC_Maj']>0)*(real_cat['Maj']>image_header['BMAJ'])
    real_cat['Resolved_flag'] = resolved_cut_real_cat

    # make sfg column
    real_cat['Simple_morphology_flag'] = np.zeros_like(real_cat['Source_id'], dtype=int)+2
    real_cat['Simple_morphology_flag'][~real_cat['Resolved_flag']] = 0
    real_cat['Simple_morphology_flag'][rows_to_remove_real_cat] = 1

    pub_cat_filename = cat_filename_base+'.pub.fits'
    real_cat.write(pub_cat_filename, format='fits', overwrite=True)
