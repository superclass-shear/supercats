import os
import copy
import pdb

import numpy as np
import numpy.fft as fft

from scipy.ndimage import fourier_gaussian
from scipy.misc import imresize

from astropy import wcs
from astropy.io import fits

from matplotlib import pyplot as plt

import cPickle as pickle

def rebin(a, shape):
    sh = shape[0],a.shape[0]//shape[0],shape[1],a.shape[1]//shape[1]
    return a.reshape(sh).sum(-1).sum(1)

def runRMSMap(config):
  rebin_factor = config.getint('rms_map', 'rebin_factor')
  input_dir = config.get('input','input_directory')
  output_dir = config.get('pipeline', 'output_dir')
  plot_dir = config.get('pipeline', 'plot_dir')
  input_image_name = config.get('input','input_image_filename')
  
  input_image_fname = input_dir+input_image_name

  header = fits.getheader(input_image_fname)
  image = fits.getdata(input_image_fname)
  image_wcs = wcs.WCS(header)

  if (image_wcs.naxis == 4):
    image_wcs = image_wcs.dropaxis(2)
    image_wcs = image_wcs.dropaxis(2)

  if len(image.shape)==4:
    image = image[0,0,:,:]

  sdat  = rebin(image,np.asarray(image.shape)/rebin_factor)
  sdat2 = rebin(image*image,np.asarray(image.shape)/rebin_factor)
  ndat = rebin_factor*rebin_factor

  rms = np.sqrt((sdat2-sdat*sdat/ndat)/ndat)*1e6
  # replace nans with 40
  rms = np.where(rms!=rms,40,rms)

  sig = config.getint('rms_map', 'fourier_smoothing')

  ftrms = fft.fft2(rms)
  ftg = fourier_gaussian(ftrms,sigma=sig)
  smo = fft.ifft2(ftg)
  smo = smo.real
  smo = smo.astype(np.float)

  if config.getboolean('rms_map', 'write_fits'):
    rmsheader = copy.copy(header)
    rmsheader['CDELT1'] = rmsheader['CDELT1']*rebin_factor
    rmsheader['CDELT2'] = rmsheader['CDELT2']*rebin_factor
    rmsheader['CRPIX1'] = rmsheader['CRPIX1']/rebin_factor
    rmsheader['CRPIX2'] = rmsheader['CRPIX2']/rebin_factor
    rmsheader['NAXIS1'] = rmsheader['NAXIS1']/rebin_factor
    rmsheader['NAXIS2'] = rmsheader['NAXIS2']/rebin_factor
    rms_wcs = wcs.WCS(rmsheader)
    hdu2 = fits.PrimaryHDU(rms, header=rmsheader)
    print('Writing rms fits to {0}'.format(config.get('rms_map', 'rms_map_filename')))
    hdu2.writeto(config.get('rms_map', 'rms_map_filename'), overwrite=True)
    
  if config.getboolean('rms_map', 'write_png'):
    
    fig = plt.figure(1, figsize=(9, 7.5))
    if (rms_wcs.naxis == 4):
      rms_plot_wcs = rms_wcs.dropaxis(2)
      rms_plot_wcs = rms_plot_wcs.dropaxis(2)
    else:
      rms_plot_wcs = rms_wcs
    
    ax = fig.add_subplot(111, projection=rms_plot_wcs)
    plt.imshow(rms,vmin=0,vmax=25,cmap='gnuplot2',origin='lower',interpolation='nearest')

    #lev = np.array([7.0, 7.5, 8.0, 9.0, 10.0])
    lev = np.array([config.getfloat('rms_map', 'mask_level')])
    cbar = plt.colorbar(orientation='vertical')
    cbar.set_label('$\sigma_{\\rm RMS} \, [\mu \mathrm{Jy}]$')
    plt.xlabel('$\mathrm{RA \, [deg]}$')
    plt.ylabel('$\mathrm{DEC \, [deg]}$')

    #plt.imshow(smo,cmap='magma')
    #cs = plt.contour(smo,lev, colors='white', linestyles='dashed')
    #p = cs.collections[0].get_paths()[0]
    #v = p.vertices
    #pickle.dump(v, open('./contour_vertices.p', 'wb'))
    cs = plt.contour(smo, colors='white')
    #plt.clabel(cs,colors='white',inline=1,fontsize=10)
    plt.title(input_image_fname.split('/')[-1].replace('_', '_'))
    map_png_fname = plot_dir+input_image_name.rstrip('.fits')+'_rms{0}x{0}.png'.format(rebin_factor)
    print('Writing rms image to {0}'.format(map_png_fname))
    plt.savefig(map_png_fname, bbox_inches='tight', dpi=300)

  if config.getboolean('rms_map', 'make_mask'):
    mask = smo < config.getfloat('rms_map', 'mask_level')

    maskheader = copy.copy(header)
    maskheader['CDELT1'] = maskheader['CDELT1']*rebin_factor
    maskheader['CDELT2'] = maskheader['CDELT2']*rebin_factor
    maskheader['CRPIX1'] = maskheader['CRPIX1']/rebin_factor
    maskheader['CRPIX2'] = maskheader['CRPIX2']/rebin_factor
    maskheader['NAXIS1'] = maskheader['NAXIS1']/rebin_factor
    maskheader['NAXIS2'] = maskheader['NAXIS2']/rebin_factor

    hdu3 = fits.PrimaryHDU(np.asarray(mask, dtype=int), header=maskheader)
    hdu3.writeto(config.get('rms_map', 'mask_filename'), overwrite=True)
